const express = require('express');

const streamingRouter = require('./routes/streaming');

const app = express();

/* configure routes */
app.use('/video-stream', streamingRouter);

module.exports = app;
