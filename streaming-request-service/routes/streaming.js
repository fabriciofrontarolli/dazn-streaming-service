const boom = require('@hapi/boom');
const UUID = require('uuid/v1');
const { isEmpty } = require('lodash');
const StreamingRouter = require('express').Router();
const RedisClient = require('../common/redisClient');

StreamingRouter.get('/reset/:user', (req, res) => {
    const { user } = req.params;

    RedisClient.set(user, JSON.stringify([]), () => {
        /* route used while in development */
        res.send(`${user} sucessfully reset`);
    });
});

StreamingRouter.get('/request/:user/:video', /* securityMiddleware, */ (req, res) => {
    const { user, video } = req.params;

    RedisClient.get(user, (err, rawUserActiveStreams) => {    
        const userActiveStreams = rawUserActiveStreams ? JSON.parse(rawUserActiveStreams) : undefined;
        const currentUserVideos = !isEmpty(userActiveStreams) ? userActiveStreams : [];

        if (err) {
            console.error('StreamingRoute: /video - Error fetching value from cache.');
            const errorResponse = boom.internal('StreamingRoute: /video - Error fetching value from cache.');
            return res.status(errorResponse.output.statusCode).send(errorResponse.output.payload);
        }

        if (currentUserVideos.find(v => v.video === video) || currentUserVideos.length >= 3) {
            const tooManyRequests = boom.tooManyRequests('You are only allowed to stream up to 3 simutaneous videos.');
            return res.status(tooManyRequests.output.statusCode).send(tooManyRequests.output.payload);
        }

        /* store information about user stream request */
        const userStreamAuthorizationKey = UUID();
        const userStreamRequest = {
            video,
            authorizationKey: userStreamAuthorizationKey,
            streamUrl: `http://localhost:10001/video-stream/stream/${user}/${video}?authorization=${userStreamAuthorizationKey}`
        };

        currentUserVideos.push(userStreamRequest);

        RedisClient.set(user, JSON.stringify(currentUserVideos), () => {
            return res.status(200).send(userStreamRequest);
        });
    });
});

module.exports = StreamingRouter;
