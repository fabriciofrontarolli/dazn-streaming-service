const fs = require('fs');
const path = require('path');
const StreamingRouter = require('express').Router();
const RedisClient = require('../common/redisClient');

const VIDEOS = [
    { type: 'music', path: path.resolve(__dirname, '../videos/music.mp4') },
    { type: 'game', path: path.resolve(__dirname, '../videos/game.mp4') },
    { type: 'boxing', path: path.resolve(__dirname, '../videos/boxing.mp4') },
];

StreamingRouter.get('/stream/:user/:video', (req, res) => {
    const { user, video } = req.params;
    const { authorization } = req.query;

    RedisClient.get(user, (err, rawUserActiveStreams) => {
        const userActiveStreams = JSON.parse(rawUserActiveStreams);
        const userStreamRequest = userActiveStreams.find(r => r.video === video);

        /* validate authorization key */
        if (!userStreamRequest || userStreamRequest.authorizationKey !== authorization) {
            const unauthorizedError = boom.unauthorized("You don't have permission to stream this video now. Try again later");
            return res.status(unauthorizedError.output.statusCode).send(unauthorizedError.output.payload);
        }

        const findSelectedVideo = VIDEOS.find(v => v.type === video)
        if (!findSelectedVideo) {
            const videoNotFoundError = boom.notFound(`The video type you are requesting does not exists. Try one of these: ${VIDEOS.map(v => v.type)}`);
            return res.status(videoNotFoundError.output.send).send(videoNotFoundError.output.payload);
        }

        fs.stat(findSelectedVideo.path, (_, stats) => {
            const { range } = req.headers;
            const { size } = stats;
            const start = Number((range || '').replace(/bytes=/, '').split('-')[0]);
            const end = size - 1;
            const chunkSize = (end - start) + 1;

            // Defining header chunks
            res.set({
                'Content-Range': `bytes ${start}-${end}/${size}`,
                'Accept-Ranges': 'bytes',
                'Content-Length': chunkSize,
                'Content-Type': 'video/mp4'
            });

            res.status(206);

            const stream = fs.createReadStream(findSelectedVideo.path, { start, end });
            stream.on('open', () => stream.pipe(res));
            stream.on('error', (streamErr) => res.end(streamErr));
            stream.on('end', (streamErr) => {
                /* remove key from cache to avoid multiple requests while streaming this */
                const updatedActiveStreams = userActiveStreams.filter(v => v.video !== video);
                RedisClient.set(user, JSON.stringify(updatedActiveStreams), () => { /* no need to wait finish persisting */  });
            });
        });
    });
});

module.exports = StreamingRouter;
