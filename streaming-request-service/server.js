const app = require('./app');

const PORT = process.env.PORT || 10000;

app.listen(PORT, '0.0.0.0', () => {
    console.log(`DAZN Video Streamer is running on port ${PORT}`);
});

process.on('uncaughtException', (error) => {
    console.error(`DAZN Streaming API Error: ${error}`)
});
