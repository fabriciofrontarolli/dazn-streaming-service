# DAZN Streamer

  

### Tech

  

DAZN Streamer uses a set of tools to work properly:

  

* [NodeJS] - As Javascript Framework

* [Express] - As Web Framework

* [Redis] - As caching layer to store user information

* [Jest] - As test runner and mock/stub tool

* [Supertest] - As API tester

* [Docker] - As image container creator - (required to run locally)

* [Docker Compose] - As container orchestrator - (required to run locally)

  

### Architecture

  

##### Streaming Request Service (localhost:10000)

  

- The service responsible for request to stream a video. User triggers (/video-stream/request/:user:video),

- User will request to stream a video (one of the types: ['music', 'game', 'boxing']).

-- http://localhost:10000/video-stream/request/fabricio/game

-- An error payload will be sent if the user reaches 3 simultaneos videos, or if the video requested is already being watched (will be available again once the stream is finished).

  

- the service will respond with a payload with the link to the video the user wants to watch e.g (http://localhost:10001/video-stream/stream/${user}/${video}?authorization=${userStreamAuthorizationKey})

  

##### Streaming Service (localhost:10001)

- The service responsible for streaming the videos

-- It validates the token send via url, checks if the token is in Redis, if so, it streams the video, otherwise it rejects the request.

  
  

![Code Coverage](https://imgur.com/c8gIsTa.png)

  
  

### Running the Project

  

DAZN Streamer requires [Node.js](https://nodejs.org/) v9+ to run.

  

Install the dependencies and devDependencies and start the server.

  

```sh

$ cd directory

$ docker-compose-up

```

  

- The app will start at port 10000

  

### Test Coverage

To get the code coverage for the app run:

  

```sh

$ cd directory

$ npm install

$ npm run test:coverage

```

  

![Code Coverage](https://imgur.com/DPn1eGb.png)

  
  

### Consideradions

  
  

### CI/CD

  

- Planned To Do: Setup a jenkins pipeline to build the app and the docker images, run the tests and deploy to a Google Instance.

--- *build*: This job build and packs the application to make it available for deployment

--- *test*: This job runs and displays test coverage for the tests in the application

--- *deploy*: This job run the app by triggering docker-compose up

  
  

### TODOs

  

- Backend (Lib)

- Write MORE Tests

- Properly document (JSDoc) each function

-

- Frontend

- Create a React client to stream videos from the service

- Create a Electron client to stream videos from the service

  

- CI/CD

- Run CI/CD Pipeline and deploy to Google instance
