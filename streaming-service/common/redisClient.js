const redis = require('redis');

const redisClient = redis.createClient(process.env.REDIS_URL);

redisClient.on('connect', () => {
    console.info('redisClient: Sucessfully connected to redis');
});

redisClient.on('error', error => {
    console.info(`redisClient: Sucessfully connected to redis - ${error}`);
});

module.exports = redisClient;
