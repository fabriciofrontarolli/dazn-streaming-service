const express = require('express');
const RedisClient = require('./common/redisClient')

/* routers */
const streamingRouter = require('./routes/streaming');

const app = express();

/* configure routes */
app.use('/video-stream', streamingRouter);

const PORT = process.env.PORT || 10001;

app.listen(PORT, '0.0.0.0', () => {
    console.log(`DAZN Video Streamer is running on port ${PORT}`);
});

process.on('uncaughtException', (error) => {
    console.error(`DAZN Streaming API Error: ${error}`)
});
