const app = require('../app');
const request = require('supertest');

describe('GET request/:user/:video', () => {
    beforeAll(() => {
        jest.enableAutomock();
    });

    afterEach(() => {
        request(app)
        .get('/video-stream/reset/fabricio')
        .set('Connection', 'keep-alive')
        .expect(200)
        .end(() => { });
    });

    afterAll(() => {
        jest.disableAutomock();
    });

    test('should sucessfully return a token for each individual type when not reached maximum streams', function(done) {
        /* Game */
        request(app)
        .get('/video-stream/request/fabricio/game')
        .set('Connection', 'keep-alive')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, gameResponse) => {

            /* Music */
            request(app)
            .get('/video-stream/request/fabricio/music')
            .set('Connection', 'keep-alive')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, musicResponse) => {

                expect(gameResponse.body.video).toBe('game')
                expect(gameResponse.body.authorizationKey).not.toBe(undefined)

                expect(musicResponse.body.video).toBe('music')
                expect(musicResponse.body.authorizationKey).not.toBe(undefined)

                done();
            });
        });
    });

    test('should return error when maximum request have been reached', function(done) {
        /* Boxing */
        request(app)
        .get('/video-stream/request/fabricio/boxing')
        .set('Connection', 'keep-alive')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, gameResponse) => {

            /* Boxing Again - Error */
            request(app)
            .get('/video-stream/request/fabricio/boxing')
            .set('Connection', 'keep-alive')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, duplicatedGameResponse) => {

                expect(gameResponse.body.video).toBe('boxing')
                expect(gameResponse.body.authorizationKey).not.toBe(undefined)

                expect(duplicatedGameResponse.body.video).toBe(undefined)
                expect(duplicatedGameResponse.body.authorizationKey).toBe(undefined)

                done();
            });
        });
    });
});
