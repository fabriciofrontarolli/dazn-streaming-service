const redis = (function() {
    var _this = {};

    return {
        createClient: () => {
            return ({
                on: (ev, cb) => cb(),
                get: (key, cb) => {
                    cb(null, _this[key]);
                },
                set: (key, value, cb) => {
                    _this[key] = value;
                    cb(null);
                },
            })
        }
    };
})();

// const redisMock = require('redis-mock');
module.exports = redis;
